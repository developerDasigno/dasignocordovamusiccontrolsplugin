# Cordova Music Controls Plugin

<img src='https://imgur.com/fh3ACOq.png' width='564' height='342'>

Music controls for Cordova applications. Display a 'media' notification with play/pause, previous, next buttons, allowing the user to control the play. Handle also headset event (plug, unplug, headset button).

## Supported platforms
- Android (4.1+)
- Windows (10+, by [filfat](https://github.com/filfat))
- iOS 8+ (by [0505gonzalez](https://github.com/0505gonzalez))

## Git orgin
Tomado del git original [homeours](https://github.com/homerours/cordova-music-controls-plugin)

## cambios latotuga
- Carga asincrónica.
- Nuevas políticas iOS.
- Abstenerse de cualquier ejecución cuando la app se está destruyendo.
